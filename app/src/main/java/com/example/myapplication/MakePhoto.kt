package com.example.myapplication

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.myapplication.databinding.CameraFragmentBinding
import kotlinx.android.synthetic.main.camera_fragment.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
typealias LumaListener = (luma: Double) -> Unit

class MakePhoto: Fragment() {
    private var _binding: CameraFragmentBinding? = null
    private var imageCapture: ImageCapture? = null
    private var handler = Handler()
    private val binding get() = requireNotNull(_binding)
    private val highlightArrows : Runnable = object : Runnable {
        override fun run() {
            //TODO: CompareSensorReadings
            Log.d("Camera", "CompareSensorReadings")
            compareSensorReadings()

            handler.postDelayed(this,200L)
        }
    }

    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService
    private lateinit var listener: TransitFragment

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as TransitFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        _binding = CameraFragmentBinding.inflate(inflater, container, false)
        return  binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val nullValues = arguments?.getFloatArray(SAVED_COORDINATES)?:0
        // Request camera permissions
        if (allPermissionsGranted())
            startCamera()
        else
            ActivityCompat.requestPermissions(mainActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)

        // Set up the listener for take photo button
        binding.cameraCaptureButton.setOnClickListener { takePhoto() }
        binding.backButton.setOnClickListener { listener.transitMenu() }
        setOutputDir()
        setCameraExecutor()

        handler.postDelayed(highlightArrows,0)

    }

    override fun onDestroyView() {
        super.onDestroy()
        handler.removeCallbacks(highlightArrows)
        cameraExecutor.shutdown()
        super.onDestroyView()
        _binding = null
    }

    private fun compareSensorReadings() {
        //TODO

        var menuFragment = MenuFragment()

        when {
            //menuFragment.nullValues[0].toInt() >= menuFragment.valuesResult[0].toInt() -> binding.
            menuFragment.nullValues[1].toInt() >= menuFragment.valuesResult[1].toInt() -> binding.topArrow.visibility.VISIBLE
            menuFragment.nullValues[2].toInt() >= menuFragment.valuesResult[2].toInt() -> binding.downArrow.visibility.VISIBLE
        }
    }

    private fun setCameraExecutor() {
        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    private fun setOutputDir() {
        outputDirectory = getOutputDirectory()
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED // ЗДЕСЬ ПОСТАВИЛ REQUIRECONTEXT ВМЕСТО baseContext
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getOutputDirectory(): File {
        val mediaDir = mainActivity().externalMediaDirs?.firstOrNull()?.let {
            File(it, "TryApp").apply { mkdirs() } }
        /*return if (mediaDir != null && mediaDir.exists())
            mediaDir else filesDir*/
        return mediaDir!!
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted())
                startCamera()
            else {
                Toast.makeText(requireContext(),"Permissions not granted by the user.", Toast.LENGTH_SHORT).show()
                mainActivity().finish()
            }
        }
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(mainActivity())
        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }

            imageCapture = ImageCapture.Builder()
                .build()

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)

            } catch(exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(mainActivity()))
    }

    private fun takePhoto() {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        // Create time-stamped output file to hold the image
        val photoFile = File(
            outputDirectory,
            SimpleDateFormat(FILENAME_FORMAT, Locale.US
            ).format(System.currentTimeMillis()) + ".jpg")

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions, ContextCompat.getMainExecutor(mainActivity()), object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)
                    val msg = "Photo capture succeeded: $savedUri"
                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show() // ЗДЕСЬ ПОСТАВИЛ REQUIRECONTEXT ВМЕСТО baseContext
                    Log.d(TAG, msg)
                }
            })
    }

    companion object {
        fun newInstance (nullValues: FloatArray) : MakePhoto {
            val fragment = MakePhoto()
            val args = Bundle()
            args.putFloatArray(SAVED_COORDINATES, nullValues)
            fragment.arguments = args
            return fragment
        }
        private const val SAVED_COORDINATES = "NULL_VALUES"
        private const val TAG = "CameraXBasic"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }
}