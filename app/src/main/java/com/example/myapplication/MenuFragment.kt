package com.example.myapplication

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.myapplication.databinding.MenuFragmBinding
import java.lang.StringBuilder

class MenuFragment : Fragment() {

    private var binding: MenuFragmBinding? = null
    private var sb = StringBuilder()
    private var rotation = 0
    private var sensorManager: SensorManager? = null
    private var r = FloatArray(9)
    private var inR = FloatArray(9)
    private var outR = FloatArray(9)
    var valuesResult = FloatArray(3)
    private var valuesResult2 = FloatArray(3)
    var nullValues = FloatArray(3)
    private var listener: SensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
        override fun onSensorChanged(event: SensorEvent) {
            when (event.sensor.type) {
                Sensor.TYPE_ACCELEROMETER -> {
                    var i = 0
                    while (i < 3) {
                        valuesAccel[i] = event.values[i]
                        i++
                    }
                }
                Sensor.TYPE_MAGNETIC_FIELD -> {
                    var i = 0
                    while (i < 3) {
                        valuesMagnet[i] = event.values[i]
                        i++
                    }
                }
            }
        }
    }
    private var handler = Handler()
    private val sensorUpdateRunnable : Runnable = object : Runnable {
        override fun run() {
            // TODO: SensorReadings
            deviceOrientation
            actualDeviceOrientation
            showInfo()
            Log.d("menu", "UpdSensorReadings")

            // repeat every 400ms
            handler.postDelayed(this, 400L)
        }
    }

    var valuesAccel = FloatArray(3)
    var valuesMagnet = FloatArray(3)
    val deviceOrientation: Unit
        get() {
            SensorManager.getRotationMatrix(r, null, valuesAccel, valuesMagnet)
            SensorManager.getOrientation(r, valuesResult)
            valuesResult[0] = Math.toDegrees(valuesResult[0].toDouble()).toFloat()
            valuesResult[1] = Math.toDegrees(valuesResult[1].toDouble()).toFloat()
            valuesResult[2] = Math.toDegrees(valuesResult[2].toDouble()).toFloat()
            return
        }
    val actualDeviceOrientation: Unit
        get() {
            SensorManager.getRotationMatrix(inR, null, valuesAccel, valuesMagnet)
            var x_axis = SensorManager.AXIS_X
            var y_axis = SensorManager.AXIS_Y
            when (rotation) {
                Surface.ROTATION_0 -> { }
                Surface.ROTATION_90 -> {
                    x_axis = SensorManager.AXIS_Y
                    y_axis = SensorManager.AXIS_MINUS_X
                }
                Surface.ROTATION_180 -> y_axis = SensorManager.AXIS_MINUS_Y
                Surface.ROTATION_270 -> {
                    x_axis = SensorManager.AXIS_MINUS_Y
                    y_axis = SensorManager.AXIS_X
                }
                else -> { }
            }
            SensorManager.remapCoordinateSystem(inR, x_axis, y_axis, outR)
            SensorManager.getOrientation(outR, valuesResult2)
            valuesResult2[0] = Math.toDegrees(valuesResult2[0].toDouble()).toFloat()
            valuesResult2[1] = Math.toDegrees(valuesResult2[1].toDouble()).toFloat()
            valuesResult2[2] = Math.toDegrees(valuesResult2[2].toDouble()).toFloat()
            return
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super .onCreateView(inflater, container, savedInstanceState)
        sensorManager = mainActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager

        return MenuFragmBinding.inflate(inflater, container, false).also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val windowManager = mainActivity().getSystemService(Activity.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        getSensorReadings()
        binding() {
            calibration.setOnClickListener { calibrationDialog() }
            makephoto.setOnClickListener { cameraDialog() }
        }
        handler.postDelayed(sensorUpdateRunnable, 0)
        rotation = display.rotation
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacks(sensorUpdateRunnable)
        sensorManager!!.unregisterListener(listener)
        binding = null
    }

    private fun binding(block: MenuFragmBinding.() -> Unit) = binding?.block()
    private fun getSensorReadings() {
        val sensorAccel: Sensor? = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        val sensorMagnet: Sensor? = sensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        sensorManager = mainActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager!!.registerListener(listener, sensorAccel, SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager!!.registerListener(listener, sensorMagnet, SensorManager.SENSOR_DELAY_NORMAL)
    }
    private fun calibrationDialog(){
        val builder = AlertDialog.Builder(mainActivity())
        builder
            .setMessage(R.string.set_null_question)
            .setPositiveButton(R.string.positive_dialog_button) { dialog, id -> saveNullSensorReadings() }
            .setNegativeButton(R.string.negative_dialog_button) { dialog, id -> dialog.dismiss() }
            .show()
    }
    private fun cameraDialog(){
        val builder = AlertDialog.Builder(mainActivity())
        builder
            .setMessage(R.string.make_photo_question)
            .setPositiveButton(R.string.make_photo) { dialog, id ->
                if(isCameraPermissionGranted()){
                    openCam()
                } else {
                    requestCamPermission()
                }
            }
            .setNegativeButton(R.string.negative_dialog_button){ dialog, id -> dialog.dismiss() }
            .show()
    }
    private fun format(values: FloatArray): String {
        return String.format("%1$.1f\t\t%2$.1f\t\t%3$.1f", values[0], values[1], values[2])
    }
    private fun showInfo() {
        sb.setLength(0)
        sb.append("""Orientation: ${format(valuesResult)}""")
        sb.append("\n")
        sb.append("""Saved data: ${format(nullValues)}""")
        binding() {
            coordinates.text = sb
        }
    }
    private fun saveNullSensorReadings() {
        nullValues[0] = valuesResult2[0]
        nullValues[1] = valuesResult2[1]
        nullValues[2] = valuesResult2[2]
    }
    private fun isCameraPermissionGranted(): Boolean {
        return activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA) } == PackageManager.PERMISSION_GRANTED
    }
    private fun requestCamPermission() {
        activity?.let { ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.CAMERA), REQUEST_CODE) }
    }
    private fun openCam() {
        val camIntent = Intent(Intent.ACTION_CAMERA_BUTTON)
        mainActivity().transitCamera(nullValues)
        //CameraManager.openCamera()
    }

    companion object {
        /*fun newInstance (nullValues: FloatArray) : MenuFragment {
            val fragment = MenuFragment()
            val args = Bundle()
            args.putFloatArray(NULLVALUE, nullValues)
            fragment.arguments = args
            return fragment
        }*/

        private const val NULLVALUE = "NULLVAL"
        private const val REQUEST_CODE = 1
    }
}