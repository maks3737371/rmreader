package com.example.myapplication

import android.hardware.SensorManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity(), TransitFragment {
    private var menuFragmentOn: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        transitMenu()
    }

    override fun transitMenu() {
        val fragment = MenuFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
        menuFragmentOn = true
    }

    override fun transitCamera(nullValues: FloatArray) {
        val fragment = MakePhoto()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
        menuFragmentOn = false
    }

    override fun onBackPressed() {
        if (!menuFragmentOn)
            transitMenu()
        else
            super.onBackPressed()
    }
}
