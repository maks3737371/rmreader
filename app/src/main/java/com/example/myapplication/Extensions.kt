package com.example.myapplication

import androidx.fragment.app.Fragment

fun Fragment.mainActivity() = requireActivity() as MainActivity         // Возвращает текущий фрагмент