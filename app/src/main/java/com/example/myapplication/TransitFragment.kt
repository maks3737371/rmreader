package com.example.myapplication

interface TransitFragment {
    fun transitMenu()
    fun transitCamera(nullValues: FloatArray)
}